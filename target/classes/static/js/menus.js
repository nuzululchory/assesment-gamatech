app.ready(function(){


        /*
        |--------------------------------------------------------------------------
        | Individual column searching
        |--------------------------------------------------------------------------
        */
        // Setup - add a text input to each footer cell
        $('menusTable tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( '<input class="form-control" type="text" placeholder="Search '+title+'">' );
        });

        // DataTable
        var table = $('menusTable').DataTable({
          'ajax': '../assets/data/json/datatables.json',
          'scrollX': true,
        });

        // Apply the search
        table.columns().every( function () {
          var that = this;

          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that.search( this.value ).draw();
            }
          });
        });




      });