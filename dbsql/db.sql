-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.11 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for test_karyawan
DROP DATABASE IF EXISTS `test_karyawan`;
CREATE DATABASE IF NOT EXISTS `test_karyawan` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `test_karyawan`;

-- Dumping structure for table test_karyawan.al_group
DROP TABLE IF EXISTS `al_group`;
CREATE TABLE IF NOT EXISTS `al_group` (
  `id_group` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `desc_group` varchar(255) DEFAULT NULL,
  `name_group` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `user_created` int(11) DEFAULT NULL,
  `user_updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table test_karyawan.al_group: ~1 rows (approximately)
DELETE FROM `al_group`;
/*!40000 ALTER TABLE `al_group` DISABLE KEYS */;
INSERT INTO `al_group` (`id_group`, `created`, `desc_group`, `name_group`, `updated`, `user_created`, `user_updated`) VALUES
	(1, '2020-07-09 12:50:58', 'Administrator', 'admin', '2020-07-09 12:51:09', 1, NULL);
/*!40000 ALTER TABLE `al_group` ENABLE KEYS */;

-- Dumping structure for table test_karyawan.al_users
DROP TABLE IF EXISTS `al_users`;
CREATE TABLE IF NOT EXISTS `al_users` (
  `id_users` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `login_status` tinyint(4) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `unique_code` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `user_created` int(11) DEFAULT NULL,
  `user_updated` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table test_karyawan.al_users: ~1 rows (approximately)
DELETE FROM `al_users`;
/*!40000 ALTER TABLE `al_users` DISABLE KEYS */;
INSERT INTO `al_users` (`id_users`, `created`, `email`, `id_group`, `login_status`, `password`, `telp`, `unique_code`, `updated`, `user_created`, `user_updated`, `username`) VALUES
	(1, '2020-07-09 12:50:33', '1', 1, 1, '$2a$10$xAsPlcqTG9bBycXSV3voHejFz8YVrZyusGB21yLA59R2bm00SF5bG', '1', '1', '2010-07-09 12:50:46', 1, NULL, 'gunawan');
/*!40000 ALTER TABLE `al_users` ENABLE KEYS */;

-- Dumping structure for table test_karyawan.t1_position
DROP TABLE IF EXISTS `t1_position`;
CREATE TABLE IF NOT EXISTS `t1_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_delete` int(11) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table test_karyawan.t1_position: ~0 rows (approximately)
DELETE FROM `t1_position`;
/*!40000 ALTER TABLE `t1_position` DISABLE KEYS */;
INSERT INTO `t1_position` (`id`, `is_delete`, `code`, `name`) VALUES
	(1, 0, 'SA', 'System Analyst'),
	(2, 0, 'BPS', 'BPS'),
	(3, 0, 'PRG', 'Programmer'),
	(4, 0, 'TEST', 'Tester'),
	(5, 0, 'HELP', 'Helpdesk');
/*!40000 ALTER TABLE `t1_position` ENABLE KEYS */;

-- Dumping structure for table test_karyawan.t2_employee
DROP TABLE IF EXISTS `t2_employee`;
CREATE TABLE IF NOT EXISTS `t2_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `birth_date` datetime DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `id_number` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2ykcumj64d6cb5u0jei1asubm` (`id_number`),
  KEY `FKlu6kxvp12604mla4h9nnt69k6` (`position_id`),
  CONSTRAINT `FKlu6kxvp12604mla4h9nnt69k6` FOREIGN KEY (`position_id`) REFERENCES `t1_position` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table test_karyawan.t2_employee: ~0 rows (approximately)
DELETE FROM `t2_employee`;
/*!40000 ALTER TABLE `t2_employee` DISABLE KEYS */;
INSERT INTO `t2_employee` (`id`, `birth_date`, `gender`, `id_number`, `name`, `position_id`, `is_delete`) VALUES
	(2, '2020-07-04 00:00:00', 2, 12345, 'Dian Bro Coy BRT', 3, 0),
	(3, '2020-01-10 00:07:00', 2, 12343, 'Dede', 1, 0),
	(4, '2020-01-15 00:07:00', 2, 123124, 'Chae', 4, 0),
	(5, '0023-07-13 00:07:00', 2, 213123132, 'CHECK', 4, 0),
	(6, '0029-07-12 00:07:00', 1, 23313132, 'Dian GY', 3, 1),
	(7, '0009-07-12 00:07:00', 1, 12313, 'COVID', 4, 1),
	(8, '2020-07-24 00:00:00', 2, 1231312, 'robot', 4, 1),
	(9, '1990-05-15 00:00:00', 1, 1321231, 'CHECK LAGI AH', 3, 1),
	(10, '2020-07-31 00:00:00', 2, 123455, 'BIBI LUNG', 4, 0);
/*!40000 ALTER TABLE `t2_employee` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
