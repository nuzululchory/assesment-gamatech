package id.co.dian.karyawan.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.dian.karyawan.model.Position;

public interface PositionRepository extends JpaRepository<Position, Integer> {

}
