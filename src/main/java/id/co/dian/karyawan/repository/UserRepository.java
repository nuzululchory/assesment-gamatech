package id.co.dian.karyawan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.dian.karyawan.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	User findByUsername(String username);

	@Query(value = "SELECT id_users FROM al_users\r\n" +
			"WHERE id_users = (\r\n" +
			"    SELECT max(id_users) FROM al_users)",nativeQuery = true)
	Integer getLastId();

	@Query(value="SELECT count(a.id_users) as id FROM al_users as a JOIN al_group as b on b.id_group = a.id_group",nativeQuery=true)
	Integer getTotal();

	@Query(value="SELECT a.id_users as id, a.username, b.id_group as idGroup, b.name_group as nameGroup, a.telp, a.email \r\n"+
			"FROM al_users as a\r\n"+
			"JOIN al_group as b on b.id_group = a.id_group\r\n"+
			"WHERE a.id_users=:id", nativeQuery = true)
	List<getAl_Users> getForEdit(Integer id);

	public static interface getAl_Users {
		String getId();
		String getUsername();
		String getIdGroup();
		String getNameGroup();
		String getTelp();
		String getEmail();
	}
}
