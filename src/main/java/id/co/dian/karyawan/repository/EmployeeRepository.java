package id.co.dian.karyawan.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.dian.karyawan.model.Employee;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	@Query("FROM Employee a WHERE a.idNumber=:number ")
	Employee getByIdNumber(Integer number);

	@Transactional
	@Modifying
	@Query("UPDATE Employee a SET a.isDelete = 1 WHERE a.id=:value")
	void softDeleteById(@Param("value") Integer id);

	@Query("FROM Employee a WHERE a.isDelete= 0 ")
	List<Employee> getByAktif();

}
