package id.co.dian.karyawan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Entity
@Data
public class Position {
        @Id
	private Integer id;

	@NotEmpty
	@Column(length = 50)
	private String code;

	@NotEmpty
	@Column(length = 100)
	private String name;
	@Column(name = "is_delete")
	private int aktif = 1;

	public Position(){

	}
	public Position(Integer id){
		this.id=id;
	}
}
