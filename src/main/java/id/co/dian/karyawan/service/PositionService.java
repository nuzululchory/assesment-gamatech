package id.co.dian.karyawan.service;

import java.util.List;

import id.co.dian.karyawan.model.Position;

public interface PositionService {

	List<Position> getAll();
}
