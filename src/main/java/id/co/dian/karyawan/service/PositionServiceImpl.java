package id.co.dian.karyawan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.dian.karyawan.model.Position;
import id.co.dian.karyawan.repository.PositionRepository;

@Service
public class PositionServiceImpl implements PositionService {

	@Autowired
	private PositionRepository repo;

	@Override
	public List<Position> getAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
