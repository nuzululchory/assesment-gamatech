package id.co.dian.karyawan.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import id.co.dian.karyawan.dto.UserDto;
import id.co.dian.karyawan.model.User;

public interface UserService extends UserDetailsService {
	
	User findByUsername(String username);
	List<User> findAll();
	User save(UserDto registrasiDto);
}
