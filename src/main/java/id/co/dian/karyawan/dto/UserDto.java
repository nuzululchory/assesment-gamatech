/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.dian.karyawan.dto;

import lombok.Data;

/**
 *
 * @author hp
 */
@Data
public class UserDto {
    
    private Integer idUsers;
    private String username;
    private String password;
    private String email;
    private String telp;
    private int userCreated;
    private int idGroup;
    
}
